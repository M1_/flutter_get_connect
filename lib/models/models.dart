import 'dart:convert';

ProdukModels produkModelsFromJson(String str) =>
    ProdukModels.fromJson(json.decode(str));

String produkModelsToJson(ProdukModels data) => json.encode(data.toJson());

class ProdukModels {
  ProdukModels({
    required this.status,
    required this.msg,
    required this.data,
  });

  bool status;
  String msg;
  List<Datum> data;

  factory ProdukModels.fromJson(Map<String, dynamic> json) => ProdukModels(
        status: json["status"],
        msg: json["msg"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "msg": msg,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    required this.variasi,
    required this.detail,
  });

  Variasi variasi;
  List<Detail> detail;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        variasi: Variasi.fromJson(json["variasi"]),
        detail:
            List<Detail>.from(json["detail"].map((x) => Detail.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "variasi": variasi.toJson(),
        "detail": List<dynamic>.from(detail.map((x) => x.toJson())),
      };
}

class Detail {
  Detail({
    required this.isLove,
    required this.produk,
    required this.produkDetail,
    required this.imgUrl,
    required this.subKategori,
  });

  int isLove;
  Produk produk;
  ProdukDetail produkDetail;
  String imgUrl;
  SubKategori subKategori;

  factory Detail.fromJson(Map<String, dynamic> json) => Detail(
        isLove: json["isLove"],
        produk: Produk.fromJson(json["Produk"]),
        produkDetail: ProdukDetail.fromJson(json["ProdukDetail"]),
        imgUrl: json["img_url"],
        subKategori: SubKategori.fromJson(json["SubKategori"]),
      );

  Map<String, dynamic> toJson() => {
        "isLove": isLove,
        "Produk": produk.toJson(),
        "ProdukDetail": produkDetail.toJson(),
        "img_url": imgUrl,
        "SubKategori": subKategori.toJson(),
      };
}

class Produk {
  Produk({
    required this.namaProduk,
    required this.namaProdukWithBrand,
  });

  String namaProduk;
  String namaProdukWithBrand;

  factory Produk.fromJson(Map<String, dynamic> json) => Produk(
        namaProduk: json["NamaProduk"],
        namaProdukWithBrand: json["NamaProdukWithBrand"],
      );

  Map<String, dynamic> toJson() => {
        "NamaProduk": namaProduk,
        "NamaProdukWithBrand": namaProdukWithBrand,
      };
}

class ProdukDetail {
  ProdukDetail({
    required this.className,
     this.material,
     this.grade,
     this.diameter,
    this.stokReady,
  });

  String className;
  String? material;
  String? grade;
  String? diameter;
  String? stokReady;

  factory ProdukDetail.fromJson(Map<String, dynamic> json) => ProdukDetail(
        className: json["ClassName"],
        material: json["Material"],
        grade: json["Grade"],
        diameter: json["Diameter"],
        stokReady: json["StokReady"],
      );

  Map<String, dynamic> toJson() => {
        "ClassName": className,
        "Material": material,
        "Grade": grade,
        "Diameter": diameter,
        "StokReady": stokReady,
      };
}

class SubKategori {
  SubKategori({
    required this.nama,
    required this.brandNama,
  });

  String nama;
  String brandNama;

  factory SubKategori.fromJson(Map<String, dynamic> json) => SubKategori(
        nama: json["Nama"],
        brandNama: json["BrandNama"],
      );

  Map<String, dynamic> toJson() => {
        "Nama": nama,
        "BrandNama": brandNama,
      };
}

class Variasi {
  Variasi({
    required this.className,
  });

  String className;

  factory Variasi.fromJson(Map<String, dynamic> json) => Variasi(
        className: json["ClassName"],
      );

  Map<String, dynamic> toJson() => {
        "ClassName": className,
      };
}
