class AppRoutes {
  static const String homepage = '/';
  static const String detail = '/detail';
  static const String login = '/login';
  static const String dashboard = '/dashboard';
  static const String news = '/news';
  static const String profile = '/profile';
  static const String favorite = '/favorite';
  // static const String  = '/';
}
