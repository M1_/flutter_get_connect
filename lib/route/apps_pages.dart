import 'package:flutter_application_3/controller/auth_controller.dart';
import 'package:flutter_application_3/moduls/dashboard/dashboard_bindings.dart';
import 'package:flutter_application_3/moduls/dashboard/dashboard_controller.dart';
import 'package:flutter_application_3/moduls/dashboard/dashboard_page.dart';
import 'package:flutter_application_3/moduls/favorite/favorite_controller.dart';
import 'package:flutter_application_3/moduls/favorite/favorite_page.dart';
import 'package:flutter_application_3/moduls/home/home_controller.dart';
import 'package:flutter_application_3/moduls/home/home_page.dart';
import 'package:flutter_application_3/moduls/details/details_page.dart';
import 'package:flutter_application_3/moduls/login/login_controller.dart';
import 'package:flutter_application_3/moduls/login/login_page.dart';
import 'package:flutter_application_3/moduls/news/news.page.dart';
import 'package:flutter_application_3/moduls/news/news_controller.dart';
import 'package:flutter_application_3/moduls/profile/profile_controller.dart';
import 'package:flutter_application_3/moduls/profile/profile_page.dart';
import 'package:flutter_application_3/route/routes.dart';
import 'package:get/get.dart';

class AppPages {
  static var list = [
    GetPage(
      name: AppRoutes.homepage,
      page: () => const HomePage(),
      binding: BindingsBuilder(() {
        // Get.put(HomeController());
        Get.lazyPut<HomeController>(() => HomeController());
      }),
    ),
    GetPage(
      name: AppRoutes.profile,
      page: () => const ProfilePage(),
      binding: BindingsBuilder(() {
        Get.put(ProfileController());
      }),
    ),
    GetPage(
      name: AppRoutes.news,
      page: () => const NewsPage(),
      binding: BindingsBuilder(() {
        Get.put(NewsController());
      }),
    ),
    GetPage(
      name: AppRoutes.favorite,
      page: () => const FavoritePage(),
      binding: BindingsBuilder(() {
        Get.put(FavoritesController());
      }),
    ),
    GetPage(
      name: AppRoutes.dashboard,
      page: () => const DashboardPage(),
      binding: DashBoardBinding(),
    ),
    GetPage(name: AppRoutes.detail, page: () => const DetailsPage()),
    GetPage(
        name: AppRoutes.login,
        page: () => const LoginPage(),
        binding: BindingsBuilder(() {
          Get.put(LoginController());
          Get.put(AuthController());
        }))
  ];
}
