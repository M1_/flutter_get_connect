import 'package:flutter_application_3/moduls/dashboard/dashboard_controller.dart';
import 'package:flutter_application_3/moduls/home/home_controller.dart';
import 'package:get/get.dart';

class DashBoardBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(DashboardController());
    Get.put(HomeController());
  }
}
