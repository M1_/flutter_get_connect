import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_application_3/models/models.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart';

class MainController extends GetxController {
  final Dio dio = Dio();
  final listdata = <ProdukModels>[].obs;

  Future<ProdukModels> getData() async {
    final response = await dio.get(
        "https://development.semangatdong.com/devpgr/api/getLovedVariasi?UserID=83");
    var data = jsonDecode(response.toString());
    if (response.statusCode == 200) {
      update();
      print(data.toString().length);

      return ProdukModels.fromJson(response.data);
    } else {
      throw Error();
    }
  }

  Future<ProdukModels> _refreshData(BuildContext context) async {
    return getData();
  }
}
