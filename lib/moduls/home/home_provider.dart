import 'dart:convert';

import 'package:flutter_application_3/models/models.dart';
import 'package:get/get_connect.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';

class HomeProvider extends GetConnect {
  Map<String, String> headers = {
    'Content-Type': 'application/json;charset=UTF-8',
    'Charset': 'utf-8'
  };
  Future<ProdukModels> fetchProducts() async {
    final response = await get(
        'https://development.semangatdong.com/devpgr/api/getLovedVariasi?UserID=83',
        headers: headers);

    if (response.status.hasError) {
      return Future.error("error");
    } else {
      print(response.body.toString());

      return ProdukModels.fromJson(jsonDecode(response.body.toString()));
    }
  }
}
