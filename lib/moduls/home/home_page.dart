import 'package:flutter/material.dart';
import 'package:flutter_application_3/moduls/home/main_controller.dart';
import 'package:flutter_application_3/route/routes.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'home_controller.dart';

class HomePage extends GetView<HomeController> {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: const Icon(FontAwesomeIcons.arrowLeft, color: Colors.black),
          title: const Text(
            "Home",
            style: TextStyle(color: Colors.black),
          ),
          elevation: 0,
          centerTitle: true,
          backgroundColor: Colors.white,
          actions: [
            IconButton(
                onPressed: () {},
                icon: const Icon(FontAwesomeIcons.ellipsisVertical,
                    color: Colors.black))
          ],
        ),
        body: controller.obx(
          (data) => Padding(
            padding: const EdgeInsets.all(8.0),
            child: GridView.builder(
                gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 3 / 3.5,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20),
                itemCount: data!.data.length,
                itemBuilder: (BuildContext ctx, index) {
                  return InkWell(
                    onTap: () {
                      Get.toNamed(AppRoutes.detail);
                    },
                    child: Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15)),
                      child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(15),
                            child: Image.network(
                              data.data[index].detail[index].imgUrl,
                              height: 120.0,
                              fit: BoxFit.cover,
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Text(
                              "Rp.300000",
                              style: TextStyle(
                                  color: Colors.pink,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          const Padding(
                              padding: EdgeInsets.only(left: 8.0, right: 8.0),
                              child: Text("bukuu bacaan bacaan bacaanbacaan ",
                                  textAlign: TextAlign.center,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis))
                        ],
                      ),
                    ),
                  );
                }),
          ),
          onEmpty: const Center(
              child: Text(
            "empty",
            style: TextStyle(color: Colors.black),
          )),
          // onError: (error)
          //   print(error.toString());
          // },

          onLoading: const Center(child: CircularProgressIndicator()),
        ));
  }
}
