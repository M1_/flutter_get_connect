import 'package:flutter_application_3/models/models.dart';
import 'package:flutter_application_3/moduls/home/home_provider.dart';
import 'package:get/get.dart';

class HomeController extends GetxController with StateMixin<ProdukModels> {
  final HomeProvider _homeProvider = HomeProvider();
  @override
  void onInit() {
    super.onInit();
    _homeProvider.fetchProducts().then((response) {
      change(response, status: RxStatus.success());
    }, onError: (err) {
      change(null, status: RxStatus.error(err.toString()));
    });
  }
}
