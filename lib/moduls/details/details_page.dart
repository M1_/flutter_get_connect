import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DetailsPage extends StatefulWidget {
  const DetailsPage({Key? key}) : super(key: key);

  @override
  State<DetailsPage> createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              FontAwesomeIcons.heart,
            ),
            onPressed: () {},
          )
        ],
      ),
      body: Column(
        children: [
          Image.network(
            "https://cepagram.com/wp-content/uploads/2020/10/Buah-buah-an.jpeg",
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height / 2,
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: 8.0, left: 25.0, right: 22.0, bottom: 8.0),
            child: Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      "Buah-Buahan Manis Terbaru Gan Manis",
                      style: TextStyle(
                          fontSize: 20.0, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 25.0, right: 25.0),
            child: const Text(
              "Dan terjadi lagi Kisah lama yang terulang kembaliKau terluka lagiDari cinta rumit yang kau jalaniAku ingin kau merasaKamu mengerti aku mengerti kamuAku ingin kau sadariCintamu bukanlah diaDengar larakuSuara hati ini memanggil namamuKar'na separuh akuDirimu",
              style:
                  TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w400),
            ),
          )
        ],
      ),
    );
  }
}
