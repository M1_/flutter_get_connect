import 'package:flutter/material.dart';
import 'package:flutter_application_3/moduls/favorite/favorite_controller.dart';
import 'package:get/get.dart';

class FavoritePage extends GetView<FavoritesController> {
  const FavoritePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: const Center(
          child: Text(
            "Favorite Page",
            style: TextStyle(fontSize: 20),
          ),
        ),
      ),
    );
  }
}
