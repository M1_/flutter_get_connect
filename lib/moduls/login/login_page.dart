import 'package:flutter/material.dart';
import 'package:flutter_application_3/moduls/dashboard/dashboard_page.dart';
import 'package:flutter_application_3/route/routes.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_application_3/moduls/login/login_controller.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final c = Get.find<LoginController>();
  // final auth = Get.find<AuthController>();

  Widget _buildTextTittle() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Text("HOME",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 25,
            )),
        Text(
          "KITCHEN",
          style: TextStyle(fontSize: 25, fontWeight: FontWeight.w200),
        )
      ],
    );
  }

  Widget _buildBottomText() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Text(
          "dont have account? ",
        ),
        Text(
          "Sign Up",
          style: TextStyle(color: Colors.pink),
        )
      ],
    );
  }

  Widget _buildSocialButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          child: FaIcon(
            FontAwesomeIcons.facebook,
            size: 35,
            color: HexColor("#3E529C"),
          ),
          onTap: () {},
        ),
        const SizedBox(
          width: 30.0,
        ),
        GestureDetector(
          child: Container(
            padding: const EdgeInsets.all(0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              border: Border.all(width: 5, color: HexColor("#40ABF0")),
              color: HexColor("#40ABF0"),
            ),
            child: FaIcon(
              FontAwesomeIcons.twitter,
              size: 23,
              color: HexColor("#FFFFFF"),
            ),
          ),
          onTap: () {},
        ),
        const SizedBox(
          width: 30.0,
        ),
        GestureDetector(
          child: Container(
            padding: const EdgeInsets.all(0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              border: Border.all(width: 5, color: HexColor("#bc2a8d")),
              color: HexColor("#bc2a8d"),
            ),
            child: FaIcon(
              FontAwesomeIcons.instagram,
              size: 23,
              color: HexColor("#FFFFFF"),
            ),
          ),
          onTap: () {},
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false, // this is new

      // appBar: AppBar(
      //   backgroundColor: Colors.white,
      //   elevation: 0,
      //   title: const Text(
      //     "SIGN IN",
      //     style: TextStyle(color: Colors.black),
      //   ),
      //   centerTitle: true,
      // ),

      body: SingleChildScrollView(
        reverse: true,
        child: Padding(
          padding: const EdgeInsets.all(28.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Padding(
                padding: EdgeInsets.only(top: 70.0),
                child: Icon(
                  Icons.home,
                  color: Colors.pink,
                  size: 100,
                ),
              ),
              _buildTextTittle(),
              const SizedBox(height: 30.0),
              TextField(
                controller: c.emailC,
                textInputAction: TextInputAction.next,
                autocorrect: false,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    hintText: "Emaill Address",
                    contentPadding:
                        const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(60.0))),
              ),
              const SizedBox(
                height: 10,
              ),
              Obx(
                () => TextField(
                  controller: c.passwordC,
                  autocorrect: false,
                  obscureText: c.hidden.value,
                  textInputAction: TextInputAction.done,
                  decoration: InputDecoration(
                      hintText: "Password",
                      contentPadding:
                          const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(60.0))),
                ),
              ),
              const SizedBox(
                height: 28,
              ),
              const Text("Forgot Password?"),
              const SizedBox(
                height: 28,
              ),
              ElevatedButton(
                onPressed: () {
                  Get.toNamed(AppRoutes.dashboard);
                },
                child: const Text('SIGN IN'),
                style: ElevatedButton.styleFrom(
                    primary: Colors.pink,
                    fixedSize: const Size(250, 50),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50))),
              ),
              const SizedBox(
                height: 28,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 54.0, right: 54.0),
                child: Row(children: const <Widget>[
                  Expanded(
                      child: Divider(
                    color: Colors.black38,
                  )),
                  Text("OR"),
                  Expanded(child: Divider(color: Colors.black38)),
                ]),
              ),
              const SizedBox(
                height: 28,
              ),
              _buildSocialButton(),
              const SizedBox(
                height: 28,
              ),
              _buildBottomText(),
              Padding(
                  // this is new
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom)),
            ],
          ),
        ),
      ),
    );
  }
}
